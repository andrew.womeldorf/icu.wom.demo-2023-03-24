package authors

import (
	"context"
	"database/sql"

	"go.uber.org/zap"
)

// AppLogic wraps the generated Querier
type AppLogic struct {
	logger  *zap.Logger
	querier Querier
}

func (a *AppLogic) CreateAuthor(ctx context.Context, arg CreateAuthorParams) (sql.Result, error) {
	a.logger.Info("hello, from AppLogic.CreateAuthor")
	return a.querier.CreateAuthor(ctx, arg)
}

func (a *AppLogic) DeleteAuthor(ctx context.Context, id int64) error {
	a.logger.Info("hello, from AppLogic.DeleteAuthor")
	return a.querier.DeleteAuthor(ctx, id)
}

func (a *AppLogic) GetAuthor(ctx context.Context, id int64) (Author, error) {
	a.logger.Info("hello, from AppLogic.GetAuthor")
	return a.querier.GetAuthor(ctx, id)
}

func (a *AppLogic) ListAuthors(ctx context.Context) ([]Author, error) {
	a.logger.Info("hello, from AppLogic.ListAuthors")
	return a.querier.ListAuthors(ctx)
}
