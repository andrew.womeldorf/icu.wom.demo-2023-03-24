module example

go 1.16

require (
	github.com/benbjohnson/litestream v0.3.9
	github.com/bufbuild/buf v1.15.1
	github.com/flowchartsman/swaggerui v0.0.0-20221017034628-909ed4f3701b
	github.com/golang-migrate/migrate/v4 v4.15.2
	github.com/grpc-ecosystem/go-grpc-middleware v1.4.0
	github.com/grpc-ecosystem/go-grpc-prometheus v1.2.0
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.15.2
	github.com/mattn/go-sqlite3 v1.14.16
	github.com/prometheus/client_golang v1.14.0
	go.uber.org/automaxprocs v1.5.2
	go.uber.org/zap v1.24.0
	golang.org/x/net v0.8.0
	google.golang.org/genproto v0.0.0-20230223222841-637eb2293923
	google.golang.org/grpc v1.53.0
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.3.0
	google.golang.org/protobuf v1.30.0
)
